﻿namespace DotNet5TestApp.Services.FileIO
{
    public interface IFileIOService
    {
        public string GetFileContents(string relativePath);
    }
}
