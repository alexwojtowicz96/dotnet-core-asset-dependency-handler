﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace DotNet5TestApp.Services.FileIO
{
    public class FileIOService : IFileIOService
    {
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<FileIOService> _logger;

        public FileIOService(IWebHostEnvironment env, ILogger<FileIOService> logger)
        {
            _env = env;
            _logger = logger;
        }

        public string GetFileContents(string relativePath)
        {
            try
            {
                var localFilePath = _env.ContentRootPath + relativePath?.Trim()?.Trim('~');
                var fileContents = File.ReadAllText(localFilePath);

                return fileContents;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                return null;
            }
        }
    }
}
