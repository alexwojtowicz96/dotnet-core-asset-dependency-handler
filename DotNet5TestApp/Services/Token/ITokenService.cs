﻿namespace DotNet5TestApp.Services.Token
{
    public interface ITokenService
    {
        string GenerateToken();
    }
}
