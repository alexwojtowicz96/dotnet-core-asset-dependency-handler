﻿namespace DotNet5TestApp.Models.Common
{
    public class AudioModel : IMediaModel
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }

        public bool HasMedia()
        {
            throw new System.NotImplementedException();
        }
    }
}
