﻿const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const del = require('del');
const fs = require('fs');
const fse = require('fs-extra');
const path = require('path');
const request = require('request');
const watch = require('gulp-watch');

const srcCssDir = "./src/css/";
const srcJsDir = "./src/js/";
const destCssDir = "./../DotNet5TestApp/wwwroot/dist/css/";
const destJsDir = "./../DotNet5TestApp/wwwroot/dist/js/";

var getAllFilesInDirectory = function (directoryName, endWith, doNotEndWith) {
    var outputFiles = [];
    var oneOffFilesDirectory = path.join(__dirname, directoryName);
    var files = fs.readdirSync(oneOffFilesDirectory);
    if (files !== undefined && files !== null && files.length > 0) {
        files.forEach(function (file) {
            if (file.endsWith(endWith) && !file.endsWith(doNotEndWith)) {
                outputFiles.push(file);
            }
        });
    }
    return outputFiles;
};

var getBaseFileName = function (file) {
    if (file === null || file === undefined || file === '') {
        return '';
    }
    return file.split('.')[0];
};

/* TASK - BUILD CSS */
gulp.task('build:css', async function () {
    console.log('Building CSS');
    gulp.src(`${srcCssDir}site.scss`)
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(concat('site.min.css'))
        .pipe(gulp.dest(destCssDir));
    var oneOffScssFiles = getAllFilesInDirectory(`${srcCssDir}component/`, '.scss', null);
    if (oneOffScssFiles.length > 0) {
        oneOffScssFiles.forEach(function (file) {
            var srcFile = `${srcCssDir}component/${file}`;
            var outputFilename = `${getBaseFileName(file)}.min.css`;
            var destinationDir = `${destCssDir}component/`;
            console.log(`Building... ${srcFile}`);
            gulp.src(srcFile)
                .pipe(sass({ outputStyle: 'compressed' }))
                .pipe(concat(outputFilename))
                .pipe(gulp.dest(destinationDir));
            console.log(outputFilename, destinationDir);
        });
    } else {
        console.log('No one-off SCSS files detected');
    }
});

/* TASK - BUILD JS */
gulp.task('build:js', async function () {
    console.log('Building JS');
    gulp.src(`${srcJsDir}site/*.js`)
        .pipe(concat('site.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(destJsDir));
    gulp.src(`${srcJsDir}core/*.js`)
        .pipe(concat('core.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(destJsDir));
    var oneOffJsFiles = getAllFilesInDirectory(`${srcJsDir}component/`, '.js', null);
    if (oneOffJsFiles.length > 0) {
        oneOffJsFiles.forEach(function (file) {
            var srcFile = `${srcJsDir}component/${file}`;
            var outputFilename = `${getBaseFileName(file)}.min.js`;
            var destinationDir = `${destJsDir}component/`;
            console.log(`Building... ${srcFile}`);
            gulp.src(srcFile)
                .pipe(concat(outputFilename))
                .pipe(uglify())
                .pipe(gulp.dest(destinationDir));
        });
    } else {
        console.log('No one-off JS files detected');
    }
});

/* TASK - BUILD */
gulp.task('build', gulp.series('build:css', 'build:js'));

/* TASK - WATCH */
gulp.task('watch', function () {
    gulp.watch(`${srcCssDir}**/*.scss`, gulp.series('build:css'));
    gulp.watch(`${srcJsDir}**/*.js`, gulp.series('build:js'));
});
