﻿using DotNet5TestApp.Services.Token;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http.Headers;

namespace DotNet5TestApp.Controllers
{
    [Route("[controller]/[action]")]
    public class DataController : Controller
    {
        private readonly ILogger<DataController> _logger;
        private readonly ITokenService _tokenService;  

        public DataController(ILogger<DataController> logger, ITokenService tokenService)
        {
            _logger = logger;
            _tokenService = tokenService;
        }

        /// <summary>
        /// GET: /Data/Ping
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Ping()
        {
            return Content("Alive", "text/plain");
        }

        /// <summary>
        /// GET: /Data/Token
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Token()
        {
            return Content(_tokenService.GenerateToken(), "text/plain");
        }
    }
}
