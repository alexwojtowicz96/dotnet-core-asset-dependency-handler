﻿using DotNet5TestApp.Services.AssetDependency;
using DotNet5TestApp.Services.FileIO;
using DotNet5TestApp.Services.Lifetime;
using DotNet5TestApp.Services.Token;
using Microsoft.Extensions.DependencyInjection;

namespace DotNet5TestApp.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServiceDependencies(this IServiceCollection services)
        {
            services.AddSingleton<ITokenService, IntTokenService>();
            services.AddSingleton<IFileIOService, FileIOService>();
            services.AddSingleton<ILifetimeService, LifetimeService>();
            services.AddScoped<IAssetDependencyService, AssetDependencyService>();

            return services;
        }
    }
}
