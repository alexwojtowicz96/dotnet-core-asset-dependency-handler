﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace DotNet5TestApp.Services.AssetDependency
{
    public class AssetDependencyService : IAssetDependencyService
    {
        private readonly IConfiguration _config;

        public ISet<string> CssDependencies { get; } = new HashSet<string>();
        public ISet<string> JsDependencies { get; } = new HashSet<string>();

        public AssetDependencyService(IConfiguration config)
        {
            _config = config;
        }

        public void AddCssDependency(string filePath)
        {
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                CssDependencies.Add(filePath);
            }
        }

        public void AddJsDependency(string filePath)
        {
            if (!string.IsNullOrWhiteSpace(filePath))
            {
                JsDependencies.Add(filePath);
            }
        }

        public void AddCssComponentDependency(string componentName)
        {
            AddCssDependency($"~/dist/css/component/{componentName}.min.css");
        }

        public void AddJsComponentDependency(string componentName)
        {
            AddJsDependency($"~/dist/js/component/{componentName}.min.js");
        }

        public void AddAssetDependencyGroup(string groupName)
        {
            var group = _config.GetSection($"AssetDependencyGroups:{groupName}")?.Get<AssetDependencyGroup>();
            var cssList = group?.css;
            var jsList = group?.js;

            if (cssList?.Any() ?? false)
            {
                foreach (var link in cssList.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    AddCssDependency(link);
                }
            }
            if (jsList?.Any() ?? false)
            {
                foreach (var link in jsList.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    AddJsDependency(link);
                }
            }
        }
    }
}
