﻿using System;

namespace DotNet5TestApp.Services.Token
{
    public class GuidTokenService : ITokenService
    {
        public string GenerateToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
