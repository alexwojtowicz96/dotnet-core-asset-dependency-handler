﻿namespace DotNet5TestApp.Models.Common
{
    public enum LinkTarget
    {
        _self,
        _blank,
        _parent
    }

    public class LinkModel
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public LinkTarget Target { get; set; } = LinkTarget._self;
        public bool HasLink { get { return !string.IsNullOrWhiteSpace(Title) && !string.IsNullOrWhiteSpace(Url); } }
    }
}
