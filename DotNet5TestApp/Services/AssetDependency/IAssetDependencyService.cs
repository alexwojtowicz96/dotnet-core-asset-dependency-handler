﻿using System.Collections.Generic;

namespace DotNet5TestApp.Services.AssetDependency
{
    public interface IAssetDependencyService
    {
        ISet<string> CssDependencies { get; }
        ISet<string> JsDependencies { get; }
        void AddCssDependency(string filePath);
        void AddJsDependency(string filePath);
        void AddCssComponentDependency(string componentName);
        void AddJsComponentDependency(string componentName);
        void AddAssetDependencyGroup(string groupName);
    }
}
