﻿using DotNet5TestApp.Services.AssetDependency;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNet5TestApp.Extensions
{
    public static class HtmlExtensions
    {
        public static string RenderJsAssetDependencies(this IHtmlHelper helper, IAssetDependencyService service, string cacheBreakerToken = null)
        {
            string output = string.Empty;
            string localFilesBundleSrc = string.Empty;

            if (service.JsDependencies?.Any() ?? false)
            {
                foreach (var dependencyFile in service.JsDependencies.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    if (dependencyFile.StartsWith("http"))
                    {
                        var src = !string.IsNullOrWhiteSpace(cacheBreakerToken) ? $"{dependencyFile}?v=${cacheBreakerToken}" : dependencyFile;
                        output += $"<script type=\"text/javascript\" src=\"{src}\"></script>";
                    }
                    else
                    {
                        localFilesBundleSrc += $"{dependencyFile}|";
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(localFilesBundleSrc))
            {
                var src = $"/AssetDependency/BundleLocalAssets?mimeType=text/css&localFiles={localFilesBundleSrc.Trim('|')}";
                src = !string.IsNullOrWhiteSpace(cacheBreakerToken) ? $"{src}&v={cacheBreakerToken}" : src;
                output += $"<script type=\"text/javascript\" src=\"{src}\"></script>";
            }

            return output;
        }

        public static string RenderCssAssetDependencies(this IHtmlHelper helper, IAssetDependencyService service, string cacheBreakerToken = null)
        {
            string output = string.Empty;
            string localFilesBundleSrc = string.Empty;

            if (service.CssDependencies?.Any() ?? false)
            {
                foreach (var dependencyFile in service.CssDependencies.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    if (dependencyFile.StartsWith("http"))
                    {
                        var src = !string.IsNullOrWhiteSpace(cacheBreakerToken) ? $"{dependencyFile}?v=${cacheBreakerToken}" : dependencyFile;
                        output += $"<link type=\"text/css\" rel=\"stylesheet\" href=\"{src}\" />";
                    }
                    else
                    {
                        localFilesBundleSrc += $"{dependencyFile}|";
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(localFilesBundleSrc))
            {
                var src = $"/AssetDependency/BundleLocalAssets?mimeType=text/css&localFiles={localFilesBundleSrc.Trim('|')}";
                src = !string.IsNullOrWhiteSpace(cacheBreakerToken) ? $"{src}&v={cacheBreakerToken}" : src;
                output += $"<link type=\"text/css\" rel=\"stylesheet\" href=\"{src}\" />";
            }

            return output;
        }
    }
}
