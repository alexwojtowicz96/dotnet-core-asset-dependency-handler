﻿using System;

namespace DotNet5TestApp.Services.Lifetime
{
    public class LifetimeService : ILifetimeService
    {
        public Guid Guid { get; set; }

        public LifetimeService()
        {
            Guid = Guid.NewGuid();
        }
    }
}
