﻿using DotNet5TestApp.Models.Common;

namespace DotNet5TestApp.Models.ViewModels
{
    public class CardViewModel : IViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public LinkModel Button { get; set; }
        public ImageModel Image { get; set; }

        public bool IsRenderable()
        {
            return !string.IsNullOrWhiteSpace(Title) || !string.IsNullOrWhiteSpace(Description) || (Button?.HasLink ?? false);
        }

        public string GetViewPath()
        {
            return "~/Views/Partials/_Card.cshtml";
        }
    }
}
