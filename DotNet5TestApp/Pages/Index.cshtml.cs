﻿using DotNet5TestApp.Models.ViewModels;
using DotNet5TestApp.Models.Common;
using DotNet5TestApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNet5TestApp.Services.Token;

namespace DotNet5TestApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly ITokenService _tokenService;

        public IList<CardViewModel> Cards { get; set; }

        public IndexModel(ILogger<IndexModel> logger, ITokenService tokenService)
        {
            _logger = logger;
            _tokenService = tokenService;
        }

        public void OnGet()
        {
            _logger.LogTrace("Requesting Index Page");

            Cards = new List<CardViewModel>()
            {
                new CardViewModel()
                {
                    Title = "Card 1",
                    Description = "This is card #1's description.",
                    Button = new LinkModel()
                    {
                        Url = "/Privacy",
                        Title = "Privacy",
                        Target = LinkTarget._self
                    },
                    Image = ImageModel.RandomImage(_tokenService)
                },
                new CardViewModel()
                {
                    Title = "Card 2",
                    Description = "This is card #2's description.",
                    Button = new LinkModel()
                    {
                        Url = "https://www.google.com",
                        Title = "Google",
                        Target = LinkTarget._blank
                    },
                    Image = ImageModel.RandomImage(_tokenService)
                }
            };
        }
    }
}
