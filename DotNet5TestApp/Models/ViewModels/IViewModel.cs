﻿namespace DotNet5TestApp.Models.ViewModels
{
    public interface IViewModel
    {
        bool IsRenderable();
        string GetViewPath();
    }
}
