﻿using DotNet5TestApp.Services.FileIO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace DotNet5TestApp.Controllers
{
    [Route("[controller]/[action]")]
    public class AssetDependencyController : Controller
    {
        private readonly ILogger<DataController> _logger;
        private readonly IFileIOService _fileIO;

        public AssetDependencyController(ILogger<DataController> logger, IFileIOService fileIO)
        {
            _logger = logger;
            _fileIO = fileIO;
        }

        /// <summary>
        /// GET: /AssetDependency/BundleLocalAssets?mimeType=text/css&localFiles=~/dist/css/component/card.min.css|~/dist/css/component/index.min.css
        /// </summary>
        /// <param name="mimeType"></param>
        /// <param name="localFiles"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult BundleLocalAssets(string mimeType, string localFiles)
        {
            var localFilePaths = localFiles?.Split('|');
            var outputContent = string.Empty;

            if (localFilePaths?.Any() ?? false)
            {
                foreach (string relativePath in localFilePaths?.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    var fileContents = _fileIO.GetFileContents($"/wwwroot{relativePath?.Trim()?.Trim('~')}");

                    if (!string.IsNullOrWhiteSpace(fileContents)) outputContent += fileContents;
                }
            }

            return Content(outputContent, mimeType);
        }
    }
}
