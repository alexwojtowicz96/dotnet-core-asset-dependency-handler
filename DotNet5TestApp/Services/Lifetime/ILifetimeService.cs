﻿using System;

namespace DotNet5TestApp.Services.Lifetime
{
    public interface ILifetimeService
    {
        Guid Guid { get; set; }
    }
}
