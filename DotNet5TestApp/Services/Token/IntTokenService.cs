﻿using System;

namespace DotNet5TestApp.Services.Token
{
    public class IntTokenService : ITokenService
    {
        public string GenerateToken()
        {
            return (new Random()).Next(0, int.MaxValue).ToString();
        }
    }
}
