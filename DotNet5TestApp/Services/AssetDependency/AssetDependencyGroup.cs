﻿using System.Collections.Generic;

namespace DotNet5TestApp.Services.AssetDependency
{
    public class AssetDependencyGroup
    {
        public IList<string> css { get; set; }
        public IList<string> js { get; set; }
    }
}
