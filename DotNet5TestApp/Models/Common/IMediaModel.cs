﻿namespace DotNet5TestApp.Models.Common
{
    public interface IMediaModel
    {
        string Url { get; set; }
        string Name { get; set; }
        string Id { get; set; }
        bool HasMedia();
    }
}
