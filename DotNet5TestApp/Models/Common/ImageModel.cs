﻿using DotNet5TestApp.Services.Token;

namespace DotNet5TestApp.Models.Common
{
    public class ImageModel : IMediaModel
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }

        public static ImageModel RandomImage(ITokenService tokenService = null)
        {
            var seed = tokenService == null ? "0" : tokenService.GenerateToken();

            return new ImageModel()
            {
                Url = $"https://picsum.photos/seed/{seed}/600/300",
                Name = $"Random Image: {seed}",
                Id = seed
            };
        }

        public bool HasMedia()
        {
            return !string.IsNullOrWhiteSpace(this.Url);
        }
    }
}
