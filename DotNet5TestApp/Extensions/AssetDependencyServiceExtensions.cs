﻿using DotNet5TestApp.Services.AssetDependency;

namespace DotNet5TestApp.Extensions
{
    public static class AssetDependencyServiceExtensions
    {
        public static void AddVueAssetDependencyGroup(this IAssetDependencyService service)
        {
            service.AddAssetDependencyGroup("vue");
        }

        public static void AddSimplebarAssetDependencyGroup(this IAssetDependencyService service)
        {
            service.AddAssetDependencyGroup("simplebar");
        }
    }
}
